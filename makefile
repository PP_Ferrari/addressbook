# ============================================================================
#  Makefile
# ============================================================================

#  Author:         (c) 2013 Pietro Paolo Ferrari
#  Created:        June 24, 2013
#  Last changed:   $Date: 2013/06/24 - 14:57 $
#  Revision:       $Revision: 1.00 $


CC = gcc
OPTI = -03
SERVER = 127.0.0.1
PORTA_SERVER = 2500

INCLUDE = include
FOLDER_SRC = src
FOLDER_BIN = bin
PATHS = -I$(INCLUDE)


all: server client



server: $(FOLDER_BIN)/server.o $(FOLDER_BIN)/utility.o $(FOLDER_BIN)/rubrica.o
	$(CC) $(OPTI) $(FOLDER_BIN)/server.o $(FOLDER_BIN)/utility.o $(FOLDER_BIN)/rubrica.o -o $(FOLDER_BIN)/server

client: $(FOLDER_BIN)/client.o $(FOLDER_BIN)/utility.o $(FOLDER_BIN)/rubrica.o
	$(CC) $(OPTI) $(FOLDER_BIN)/client.o $(FOLDER_BIN)/utility.o $(FOLDER_BIN)/rubrica.o -o $(FOLDER_BIN)/client




$(FOLDER_BIN)/server.o: $(FOLDER_SRC)/server.c $(FOLDER_SRC)/utility.h $(FOLDER_SRC)/rubrica.h
	$(CC) $(OPTI) $(PATHS) $(FOLDER_SRC)/server.c  -c -o $(FOLDER_BIN)/server.o


$(FOLDER_BIN)/client.o: $(FOLDER_SRC)/client.c $(FOLDER_SRC)/utility.h $(FOLDER_SRC)/rubrica.h
	$(CC) $(OPTI) $(PATHS) $(FOLDER_SRC)/client.c  -c -o $(FOLDER_BIN)/client.o


$(FOLDER_BIN)/utility.o: $(FOLDER_SRC)/utility.c $(FOLDER_SRC)/utility.h $(FOLDER_SRC)/rubrica.h
	$(CC) $(OPTI) $(PATHS) $(FOLDER_SRC)/utility.c  -c -o $(FOLDER_BIN)/utility.o

    
$(FOLDER_BIN)/rubrica.o: $(FOLDER_SRC)/rubrica.c $(FOLDER_SRC)/rubrica.h
	$(CC) $(OPTI) $(PATHS) $(FOLDER_SRC)/rubrica.c  -c -o $(FOLDER_BIN)/rubrica.o
    
    
    
    
run_server:
	$(FOLDER_BIN)/server -p $(PORTA_SERVER)

run_client:
	$(FOLDER_BIN)/client -a $(SERVER) -p $(PORTA_SERVER)




.phony: clean

clean:
	rm -f ./client ./server $(FOLDER_BIN)/*.o $(FOLDER_BIN)/*.s
    
    