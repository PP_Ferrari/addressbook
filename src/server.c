

#include <sys/socket.h>       /*  socket definitions        */
#include <sys/types.h>        /*  socket types              */
#include <arpa/inet.h>        /*  inet (3) funtions         */
#include <unistd.h>           /*  misc. UNIX functions      */

#include "utility.h"
#include "rubrica.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>


/*  Global constants  */

#define MAX_DIM             1024
#define MAX_LINE            1000
#define CODA                   3


int       list_s;                /*  listening socket          */
int       conn_s;                /*  connection socket         */


void close_all (int signal) {
    
    close(conn_s);
    close(list_s);

    printf("\n\nInterreput Signal...... \n");
    printf("====> Socket chiuso\n");
    printf("====> Server spento\n");
    
    exit(0);
}





int main(int argc, char *argv[]) {
    
    
    short int port;                  /*  port number               */
    struct    sockaddr_in servaddr;  /*  socket address structure  */
    struct	  sockaddr_in their_addr;
    char      buffer[MAX_LINE];      /*  character buffer          */
    char      *endptr;                /*  for strtol()              */
	char      buff[MAX_DIM];
    int 	  sin_size, number_of_result;
    char      *response;
    char      *orario;
    struct timeval timeout;
    timeout.tv_sec = 10;
    timeout.tv_usec = 0;
    
    Contatto *contatto;
    Utente   *utente;
    

    contatto        = malloc(sizeof(Contatto));
    utente          = malloc(sizeof(Utente));
    response        = malloc(sizeof(char) * 2);
    orario          = malloc(sizeof(MAX_DIM));
    
    
    printf("\n\n***** BENVENUTO NEL SERVER *****\n\n");
    
    
    /*  Get command line arguments  */
    ParseCmdLineServer(argc, argv, &endptr);
    
    
	port = strtol(endptr, &endptr, 0);
	if ( *endptr ) {
	    fprintf(stderr, "server: porta non riconosciuta.\n");
	    exit(EXIT_FAILURE);
	}
    
    
    
	/*  Create the listening socket  */
    if ( (list_s = socket(AF_INET, SOCK_STREAM, 0)) < 0 ) {
		fprintf(stderr, "server: errore nella creazione della socket.\n");
		exit(EXIT_FAILURE);
    }
    
    /* NON FUNZIONANO */
    setsockopt ( list_s, SOL_SOCKET, SO_SNDTIMEO, ( struct timeval * ) &timeout,sizeof ( struct timeval ) );
    setsockopt ( list_s, SOL_SOCKET, SO_RCVTIMEO, ( struct timeval * ) &timeout,sizeof ( struct timeval ) );
    
    
    
    /*  Set all bytes in socket address structure to
     zero, and fill in the relevant data members   */
    memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port        = htons(port);
    

    
    /*  Bind our socket addresss to the
     listening socket, and call listen()  */
    if ( bind(list_s, (struct sockaddr *) &servaddr, sizeof(servaddr)) < 0 ) {
		fprintf(stderr, "server: errore durante la bind.\n");
		exit(EXIT_FAILURE);
    }
    
    if ( listen(list_s, CODA) < 0 ) {
		fprintf(stderr, "server: errore durante la listen.\n");
		exit(EXIT_FAILURE);
    }
    
    
    // Cattura il comando CTRL+C, chiudendo la connessione;
    signal(SIGINT, close_all);
    
    
   
    
    /*  Enter an infinite loop to respond
     to client requests and echo input  */
    while ( 1 ) {
        
		/*  Wait for a connection, then accept() it  */
		sin_size = sizeof(struct sockaddr_in);
		if ( (conn_s = accept(list_s, (struct sockaddr *)&their_addr, &sin_size) ) < 0 ) {
		    fprintf(stderr, "server: errore nella accept.\n");
	    	exit(EXIT_FAILURE);
		}
        
        
        
        
        
        read(conn_s, utente, sizeof(Utente));
        response = autentica_utente(utente);
        write(conn_s, response, MAX_DIM);
        
        if ( *response == 'N' ) {
            printf("errore nell'autenticazione!\n");
            exit(EXIT_FAILURE);
        }
        
        write(conn_s, utente->permessi, MAX_DIM);
        
        
        do {
            
            
            
            read(conn_s, buff, MAX_DIM);
            if ( strcmp(buff, "ADD") == 0 ) {
                
                printf("[%s] %s vuole aggiungere il seguente contatto: \n\n", orario, utente->username);
                read(conn_s, contatto, sizeof(Contatto));
                
                if (setsockopt (list_s, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout, sizeof(timeout)) < 0) {
                    printf("setsockopt failed\n");
                    exit(0);
                }
                
                printf("  => Nome: %s\n", contatto->nome);
                printf("  => Cognome: %s\n", contatto->cognome);
                printf("  => Numero: +39 %s\n", contatto->numero);
                
                response = aggiungi_contatto(contatto);
                if ( *response == 'Y' ) {
                    printf("====> esito operazione: riuscita \n\n");
                    write(conn_s, response, MAX_DIM);
                } else {
                    printf("====> esito operazione: fallita \n\n");
                    write(conn_s, response, MAX_DIM);
                }
            }
            
            if ( strcmp(buff, "SEARCH") == 0 ) {
                
                
                read(conn_s, buff, MAX_DIM);
                
                if ( strcmp(buff, "NOM") == 0 ) {
                    read(conn_s, buff, MAX_DIM);
                    printf("===> server says: ricerco contatto per nome (valore passato: %s)\n", buff);
                    number_of_result = ricerca_contatto_per_nome(conn_s, buff);
                    printf("===> server says: numero di corrispondenza trovate = %d\n\n", number_of_result);
                }
                else if ( strcmp(buff, "COG") == 0 ) {
                    read(conn_s, buff, MAX_DIM);
                    printf("===> server says: ricerco contatto per cognome (valore passato: %s)\n", buff);
                    number_of_result = ricerca_contatto_per_cognome(conn_s, buff);
                    printf("===> server says: numero di corrispondenza trovate = %d\n\n", number_of_result);
                }
                else if ( strcmp(buff, "TEL") == 0 ) {
                    read(conn_s, buff, MAX_DIM);
                    printf("===> server says: ricerco contatto per numero (valore passato: %s)\n", buff);
                    number_of_result = ricerca_contatto_per_numero(conn_s, buff);
                    printf("===> server says: numero di corrispondenza trovate = %d\n\n", number_of_result);
                }
                
            
            }
            
        } while( strcmp(buff, "quit") != 0 );
        
        orario = stampa_orario();
        write(conn_s, "   Arrivederci!", MAX_DIM);
        printf("\nDisconnessione...\n");
        printf("%s%s si è disconnesso\n", orario, utente->username);
        
        
        /*  Close the connected socket  */
        if ( close(conn_s) < 0 ) {
            fprintf(stderr, "server: errore durante la close.\n");
            exit(EXIT_FAILURE);
                  
        }
    }
              
    if ( close(list_s) < 0 ) {
       fprintf(stderr, "server: errore durante la close.\n");
       exit(EXIT_FAILURE);
    }
    
    
    /* libero memoria */
    free(utente);
    free(response);
    free(contatto);
}




              
              
              
              
              
              
