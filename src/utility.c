
#include <sys/socket.h>       /*  socket definitions        */
#include <sys/types.h>        /*  socket types              */
#include <arpa/inet.h>        /*  inet (3) funtions         */
#include <unistd.h>           /*  misc. UNIX functions      */

#include "utility.h"
#include "rubrica.h"

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>


#define DEBUG


void stampa_info_contatto(Contatto* contatto);


// ============================================================================
// void stampa_messaggio_benvenuto();
// ============================================================================
void stampa_messaggio_benvenuto() {
    
    printf("\n\n");
    printf("Benvenuto nella Rubrica\n\n");
}


// ============================================================================
// void stampa_dati_utente(char *utente);
// ============================================================================
void stampa_dati_utente(char *utente) {
    
    printf("Benvenuto %s", utente);
    
}



// ============================================================================
// void stampa_menu(char* permessi);
// ============================================================================
void stampa_menu(char* permessi) {
    
    
    printf("\n\n\n\n\n");
    printf("***********************\n");
    printf("\t Menu \n");
    printf("***********************\n");
    
    /* SOLO LETTURA */
    
    if ( strcmp(permessi, "r") == 0 ) {
        printf("*\n");
        printf("  SEARCH - Cerca contatto \n");
        printf("  quit - Esci \n");
        printf("*\n");
    }
    
    if ( strcmp(permessi, "wr") == 0 ) {
        printf("\n");
        printf("  ADD      -   Aggiungi contatto nella rubrica \n");
        printf("  SEARCH   -   Cerca contatto nella rubrica\n");
        printf("  quit     -   Esci dalla rubrica \n");
        printf("\n");
    }

    
    printf("***********************\n\n");
    printf("(esempio: per cercare un utente, digitare SEARCH)\n");
    printf("\nDigita codice opzione scelta: ");
}



// ============================================================================
// void stampa_schermata_aggiunta_contatto();
// ============================================================================
void stampa_schermata_aggiunta_contatto() {
    
    printf("\n\n Aggiunta di un contatto ");
    return;
    
}



// ============================================================================
// void stampa_info_contatto(Contatto* contatto);
// ============================================================================
void stampa_info_contatto(Contatto* contatto) {
    
    printf("  => Nome: %s\n", contatto->nome);
    printf("  => Cognome: %s\n", contatto->cognome);
    printf("  => Numero: +39 %s\n", contatto->numero);
    printf("\n\n");
    
}




// ============================================================================
// void stampa_schermata_cerca_contatto();
// ============================================================================
void stampa_schermata_cerca_contatto() {
    
    printf("\n\nRicerca di un contatto ");
    printf("   ==> NOM - Nome\n");
    printf("   ==> COG - Cognome\n");
    printf("   ==> TEL - Numero di telefono");
    
    printf("***********************\n\n");
    printf("(esempio: per cercare un utente per NOME, digitare NOM)\n");
    printf("\nDigita codice opzione scelta: ");
    return;
    
}



// ============================================================================
// char* stampa_orario();
// ============================================================================
char* stampa_orario() {
    
    time_t current_time;
    char* c_time_string;
    
    c_time_string = malloc(sizeof(1024));
    
    /* Obtain current time as seconds elapsed since the Epoch. */
    current_time = time(NULL);
    
    /* Convert to local time format. */
    c_time_string = ctime(&current_time);
    
    return c_time_string;
}





// ============================================================================
// int ParseCmdLineClient(int argc, char *argv[], char **szAddress, char **szPort)
// ============================================================================
int ParseCmdLineClient(int argc, char *argv[], char **szAddress, char **szPort) {
    int n = 1;
    
    while ( n < argc )
	{
		if ( !strncmp(argv[n], "-a", 2) || !strncmp(argv[n], "-A", 2) )
		{
		    *szAddress = argv[++n];
		}
		else
			if ( !strncmp(argv[n], "-p", 2) || !strncmp(argv[n], "-P", 2) )
			{
			    *szPort = argv[++n];
			}
			else
				if ( !strncmp(argv[n], "-h", 2) || !strncmp(argv[n], "-H", 2) )
				{
		    		printf("Sintassi:\n\n");
			    	printf("    client -a (indirizzo server) -p (porta del server) [-h].\n\n");
			    	exit(EXIT_SUCCESS);
				}
		++n;
    }
	if (argc==1)
	{
   		printf("Sintassi:\n\n");
    	printf("    client -a (indirizzo server) -p (porta del server) [-h].\n\n");
	    exit(EXIT_SUCCESS);
	}
    return 0;
}



// ============================================================================
// int ParseCmdLineServer(int argc, char *argv[], char **szPort)
// ============================================================================
int ParseCmdLineServer(int argc, char *argv[], char **szPort) {
    int n = 1;
    
    while ( n < argc )
	{
		if ( !strncmp(argv[n], "-p", 2) || !strncmp(argv[n], "-P", 2) )
			*szPort = argv[++n];
		else
			if ( !strncmp(argv[n], "-h", 2) || !strncmp(argv[n], "-H", 2) )
			{
			    printf("Sintassi:\n\n");
	    		printf("    server -p (porta) [-h]\n\n");
			    exit(EXIT_SUCCESS);
			}
		++n;
    }
	if (argc==1)
	{
	    printf("Sintassi:\n\n");
		printf("    server -p (porta) [-h]\n\n");
	    exit(EXIT_SUCCESS);
	}
    return 0;
}
