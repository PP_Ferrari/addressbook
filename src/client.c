

/*
 CLIENT.C
*/


#include <sys/socket.h>       /*  socket definitions        */
#include <sys/types.h>        /*  socket types              */
#include <arpa/inet.h>        /*  inet (3) funtions         */
#include <unistd.h>           /*  misc. UNIX functions      */
#include <netinet/in.h>
#include <netdb.h>

#include "utility.h"
#include "rubrica.h"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>


/*  Global constants  */

#define MAX_LINE            1000
#define MAX_DIM             1024

int       conn_s;                /*  connection socket         */






//
// ============================================================================
// void close_connection_from_client (int signal)
// ============================================================================
//

void close_connection_from_client (int signal) {
    
    char* r;
    
    write(conn_s, "quit", MAX_DIM);
    close(conn_s);
    
    printf("\n\nInterreput Signal...... \n");
    printf("====> Connession con il server chiusa \n");
    
    exit(0);
}






//
// ============================================================================
// void close_connection_from_server (int signal)
// ============================================================================
//

void close_connection_from_server (int signal) {
    
    char* r;
    
    close(conn_s);
    
    printf("\n\nIl server ha chiuso la connessione... \n");
    printf("====> Connession con il server chiusa \n");
    
    exit(0);
}







//
// ============================================================================
// int main(int argc, char *argv[]) 
// ============================================================================
//

int main(int argc, char *argv[]) {
    
    int         scelta;
    short int   port;                   /*  port number               */
    struct      sockaddr_in servaddr;   /*  socket address structure  */
    char        buffer[MAX_DIM];        /*  character buffer          */
    char        *szAddress;             /*  Holds remote IP address   */
    char        *szPort;                /*  Holds remote port         */
    char        *endptr;                /*  for strtol()              */
	struct      hostent *he;
    char        *response, *permessi;
    struct      timeval timeout;
    
    
    
    timeout.tv_sec = 10;
    timeout.tv_usec = 0;
    
    Contatto    *contatto;
    Utente      *utente_connesso;
    
    contatto        = malloc(sizeof(Contatto));
    
    utente_connesso = malloc(sizeof(Utente));
    response        = malloc(sizeof(char) * 2);
    permessi        = malloc(sizeof(char) * 3);
    
    
	he = NULL;
    
    /*  Get command line arguments  */
    
    ParseCmdLineClient(argc, argv, &szAddress, &szPort);
    
    
    /*  Set the remote port  */
    
    port = strtol(szPort, &endptr, 0);
    if ( *endptr )
	{
		printf("client: porta non riconosciuta.\n");
		exit(EXIT_FAILURE);
    }
	
    
    /*  Create the listening socket  */
    
    if ( (conn_s = socket(AF_INET, SOCK_STREAM, 0)) < 0 )
	{
		fprintf(stderr, "client: errore durante la creazione della socket.\n");
		exit(EXIT_FAILURE);
    }
    
    
    
    /* NON FUNZIONANO e non so il perché <.< */
    //setsockopt ( conn_s, SOL_SOCKET, SO_SNDTIMEO, ( struct timeval * ) &timeout,sizeof ( struct timeval ) );
    //setsockopt ( conn_s, SOL_SOCKET, SO_RCVTIMEO, ( struct timeval * ) &timeout,sizeof ( struct timeval ) );
    
    
    
    
    /*  Set all bytes in socket address structure to
     zero, and fill in the relevant data members   */
	memset(&servaddr, 0, sizeof(servaddr));
    servaddr.sin_family      = AF_INET;
    servaddr.sin_port        = htons(port);
    
    
    
    
    /*  Set the remote IP address  */
    
    if ( inet_aton(szAddress, &servaddr.sin_addr) <= 0 )
	{
		printf("client: indirizzo IP non valido.\nclient: risoluzione nome...");
		
		if ((he = gethostbyname(szAddress) ) == NULL)
		{
			printf("fallita.\n");
  			exit(EXIT_FAILURE);
		}
		printf("riuscita.\n\n");
		servaddr.sin_addr = *((struct in_addr *)he->h_addr);
    }
	
    
    
    /*  connect() to the remote echo server  */
    
    if ( connect(conn_s, (struct sockaddr *) &servaddr, sizeof(servaddr) ) < 0 )
	{
		printf("client: errore durante la connect.\n");
		exit(EXIT_FAILURE);
    }

    signal(SIGINT, close_connection_from_client);
    signal(SIGPIPE, close_connection_from_server);
    
    
    
    stampa_messaggio_benvenuto();
    
    printf("Inserire username: ");
    scanf("%s", utente_connesso->username);
    
    printf("Inserire password: ");
    scanf("%s", utente_connesso->password);
    
    write(conn_s, utente_connesso, sizeof(Utente));
    read(conn_s, response, MAX_DIM);
    
    if ( *response == 'N' ) {
        printf("client: errore durante il login.\n");
        exit(0);
    } else {
        
        read(conn_s, permessi, MAX_DIM);
        strcpy(utente_connesso->permessi, permessi);
        
        stampa_dati_utente(utente_connesso->username);
     
        do {
            
            stampa_menu(utente_connesso->permessi);
            
            scanf("%s", buffer);
            write(conn_s, buffer, MAX_DIM);
        
            if ( strcmp(buffer, "ADD") == 0 ){
            
                stampa_schermata_aggiunta_contatto();
            
                printf("\n  -> Nome: ");
                scanf("%s", contatto->nome);
                printf("  -> Cognome: ");
                scanf("%s", contatto->cognome);            
                printf("  -> Numero: ");
                scanf("%s", contatto->numero);
            
                /* invio i dati memorizzati nella struct */
                write(conn_s, contatto, sizeof(Contatto));
                
                /* leggo dal buffer il risultato dell'operazione */
                read(conn_s, response, MAX_DIM);
                
                /* a seconda dell'esito, mando due output diversi */
                if ( *response == 'Y' ) {
                    printf("\n\nContatto aggiunto con **successo**!\n");
                    printf("Ora tornerai al menu principale!\n");
                } else {
                    printf("\n Attenzione! C'è stato un errore ed il contatto non è stato aggiungo!\n");
                    printf("Ora tornerai al menu principale!\n");
                }
                
            
        
            }
            else if ( strcmp(buffer, "SEARCH") == 0) {
           
                stampa_schermata_cerca_contatto();
                // scelgo la tipologia di ricerca da fare;
                scanf("%s", buffer);
                
                // la invio al server;
                write(conn_s, buffer, MAX_DIM);
                
                // invio la string a seconda della scelta fatta precedentement;
                if ( strcmp(buffer, "NOM") == 0 ) {
                    printf("\nDigitare il nome: ");
                }
                else if ( strcmp(buffer, "COG") == 0 ) {
                    printf("\nDigitare il cognome: ");
                }
                else if ( strcmp(buffer, "TEL") == 0 ) {
                    printf("\nDigitare il numero di telefono: ");
                }
                
                // invio la stringa da cercare;
                scanf("%s", buffer);
                write(conn_s, buffer, MAX_DIM);
                
                
                printf("leggo la lista ricevuta dal server...\n\n");
                
                do {
                    
                    read(conn_s, buffer, MAX_DIM);
                    if ( strcmp(buffer, "end") == 0 )
                        contatto = NULL;
                    else {
                        read(conn_s, contatto, sizeof(Contatto));
                        stampa_info_contatto(contatto);
                    }
                } while ( contatto );

                printf("Ora tornerai al menu principale!\n");
                
        
            }
            else if ( strcmp(buffer, "quit") != 0 && strcmp(buffer, "ADD") != 0 && strcmp(buffer, "SEARCH") != 0 ) {
                
                printf("\n");
                printf("*** ATTENZIONE ***\n");
                printf("Hai inserito un comanda sbagliato, riprova...\n\n");
                
            }
            
        } while ( strcmp(buffer, "quit") != 0 );
    }
    
    
    read(conn_s, buffer, MAX_DIM);
    printf("\n\n\n*******************************************\n");
    printf("\t %s \n", buffer);
    printf("*******************************************\n\n");

    
    close(conn_s);
    
    /* libero memoria */
    free(utente_connesso);
    free(response);
    free(contatto);
    free(permessi);
}


/* funzione per la stampa di un messaggio di connessione avvenuta con il server
 della rubrica */


