

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "rubrica.h"
#include "utility.h"

// #define DEBUG
#define MAX_DIM 1024

// ============================================================================
// char* autentica_utente(Utente* utente);
// ============================================================================

char* autentica_utente(Utente* utente) {
    
    FILE*       file_login; /* puntatore al file */
    Utente*     temp_user;  /* user_temporaneo */
    char*       result;     /* variabile locale per salvare il risultato */
    
    temp_user   = malloc(sizeof(Utente));
    result      = malloc(sizeof(char) * 2);
    
    #ifdef DEBUG
    printf("\n ******* sono nella funzione AUTENTICA UTENTE ********** \n\n");
    #endif
    
    
    if ( (file_login = fopen("file/login.txt", "r")) == NULL ) {
        printf("Il file non può essere aperto");
        exit(0);
    }
    else {
        
        
        // leggo i dati dal file e li confronto con quelli passati come argomento;
        while ( !feof(file_login) ) {
                        
            fscanf(file_login, "%s%s%s", temp_user->username, temp_user->password, temp_user->permessi);
            
            #ifdef DEBUG
            printf("\nUsername nel file ====> %s", temp_user->username);
            printf("\nPassword nel file ====> %s", temp_user->password);
            #endif
            
            if (
                 strcmp(temp_user->username, utente->username) == 0 &&
                 strcmp(temp_user->password, utente->password) == 0
               ) {
                
                printf("\n\nConnessione avvenuta...\n");
                printf("Dati utente connesso: \n");
                printf("  ==> username: %s\n", utente->username);
                printf("  ==> permessi: %s\n", temp_user->permessi);
                printf("\n\n");
                strcpy(utente->permessi, temp_user->permessi);
                
                *result = 'Y';
                return result;
            }
        
        }
        
    }
        
    /* chiudo il file */
    fclose(file_login);
    
    
    printf("\nNON HO TROVATO NULLA\n");
    // se sono arrivato qui non ho trovato corrispondenza e ritorno -1;
    *result = 'N';
    free(temp_user);
    return result;
}



//
// ============================================================================
// int aggiungi_contatto(Contatto* contatto);
// ============================================================================
//


char* aggiungi_contatto(Contatto* contatto) {
    
    
    FILE* file;
    char* result;
    
    result = malloc(sizeof(char) * 2);

    
    if ( (file = fopen("file/rubrica.txt", "a")) == NULL ) {
        *result = 'N';
        return result;
    }
    
    else {
        
        fprintf(file, "%s\t\t%s\t\t%s\n", contatto->nome, contatto->cognome, contatto->numero);
        fclose(file);
        *result = 'Y';
        return result;
    }
    
    *result = 'N';
    return result;
}




//
// ============================================================================
// Contatto* ricerca_contatto_per_nome(char* nome);
// ============================================================================
//


int ricerca_contatto_per_nome(int conn_s, char* nome) {
    
    
    FILE* file;
    Contatto *temp_contatto;
    int cont = 0;
    
    temp_contatto = malloc(sizeof(Contatto));
    
    
    // apro il file in modalità lettura;
    if ( (file = fopen("file/rubrica.txt", "r")) == NULL ) {
        exit(0);
    }
    else {
        while ( !feof(file) ) {
            
            fscanf(file, "%s%s%s", temp_contatto->nome, temp_contatto->cognome, temp_contatto->numero);
            
            if ( strcmp(nome, temp_contatto->nome) == 0 ) {
                
                cont++;
                write(conn_s, "ok", MAX_DIM);
                write(conn_s, temp_contatto, sizeof(Contatto));
                
            }
        }
        
        
        write(conn_s, "end", MAX_DIM);
        return cont;
    }


}





//
// ============================================================================
// Contatto* ricerca_contatto_per_cognome(char* cognome);
// ============================================================================
//


int ricerca_contatto_per_cognome(int conn_s, char* cognome) {
    
        
    FILE* file;
    Contatto *temp_contatto;
    int cont = 0;

    temp_contatto = malloc(sizeof(Contatto));
    
    
    // apro il file in modalità lettura;
    if ( (file = fopen("file/rubrica.txt", "r")) == NULL ) {
        exit(0);
    }
    else {
        while ( !feof(file) ) {
            
            fscanf(file, "%s%s%s", temp_contatto->nome, temp_contatto->cognome, temp_contatto->numero);
            
            if ( strcmp(cognome, temp_contatto->cognome) == 0 ) {
                
                cont++;
                write(conn_s, "ok", MAX_DIM);
                write(conn_s, temp_contatto, sizeof(Contatto));
                
            }
        }
    
        
        write(conn_s, "end", MAX_DIM);
        return cont;
    }
}




//
// ============================================================================
// Contatto* ricerca_contatto_per_numero(char* numero);
// ============================================================================
//


int ricerca_contatto_per_numero(int conn_s, char* numero) {
    
    FILE* file;
    Contatto *temp_contatto;
    int cont = 0;
    
    temp_contatto = malloc(sizeof(Contatto));
    
    
    // apro il file in modalità lettura;
    if ( (file = fopen("file/rubrica.txt", "r")) == NULL ) {
        exit(0);
    }
    else {
        while ( !feof(file) ) {
            
            fscanf(file, "%s%s%s", temp_contatto->nome, temp_contatto->cognome, temp_contatto->numero);
            
            if ( strcmp(numero, temp_contatto->numero) == 0 ) {
                
                cont++;
                write(conn_s, "ok", MAX_DIM);
                write(conn_s, temp_contatto, sizeof(Contatto));
                
            }
        }
        
        
        write(conn_s, "end", MAX_DIM);
        return cont;
    }

}
