
#ifndef _rubrica_h
#define _rubrica_h
#endif



/*
 
 strutture d'utilizzo
 
 
*/




typedef struct contatto {
    char nome[50];
    char cognome[50];
    char numero[20];
} Contatto;


/*
 permessi utente:
  - "r": solo lettura, permette di cercare all'interno della rubrica;
  - "wr": scrittura e lettura, permette di inserire e cercare all'interno della rubrica;
*/

typedef struct utente {
    char username[50];
    char password[20];
    char permessi[3];
} Utente;



/*
 # ============================================================================
 #  autentica_utente(Utente* user)
 # ============================================================================
 nome: autentica_utente();
 parametri: puntatore ad un oggetto Utente;
 scopo: server per autentica l'utente e sapere se è registrato; se sì, verranno salvati
 in una variabile i suoi permessi di scrittura e/o lettura sul server;
 return: un puntatore ad una struttura utente;

*/

char* autentica_utente(Utente* user);

/*
 # ============================================================================
 #  aggiungi_contatto(Contatto* contatto)
 # ============================================================================
 */

char* aggiungi_contatto(Contatto* contatto);


/*
 # ============================================================================
 #  ricerca_contatto_per_nome(char* nome)
 # ============================================================================
 */



int ricerca_contatto_per_nome(int conn_s, char* nome);
int ricerca_contatto_per_cognome(int conn_s, char* cognome);
int ricerca_contatto_per_numero(int conn_s, char* numero);





